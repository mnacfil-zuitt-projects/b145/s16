
// Activity:
// 1. In the S16 folder, create an activity folder, an index.html file inside of it and link the index.js file.
// 2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
console.log('Hello World')
// 3. Create two variables that will store two different number values and create several variables that will store the sum, difference, product and quotient of the two numbers. Print the results in the console
let num1 = 1000;
let num2 = 200;
const sum = num1 + num2;
const difference = num1 - num2;
const product = num1 * num2;
const quotient = num1 / num2;
console.log(`The sum of two numbers is: ${sum}`);
console.log(`The difference of two numbers is : ${difference}`);
console.log(`The sum of two numbers is: ${product}`);
console.log(`The sum of two numbers is: ${quotient}`);
// 4. Using comparison operators print out a string in the console that will return a message if the sum is greater than the difference.
let isGreather = sum !== difference
console.log(`The sum is greater than the difference: ${isGreather}`)
// 5. Using comparison and logical operators print out a string in the console that will return a message if the product and quotient are both positive numbers.
let isProduct = product !== 0;
let isQuotient = quotient !== 0;
let isLogical = isProduct && isQuotient;
let isPositiveNumbers = isProduct && isQuotient;
console.log(`The product and quotient are positive numbers: ${isPositiveNumbers}`)
// 6. Using comparison and logical operators print out a string in the console that will return a message if one of the results is a negative number.
let bool1 = sum === -100000; // comparing if sum is stricly equl to negative number (any negative value) and so on..
let bool2 = difference === -100000;
let bool3 = product === -100000;
let bool4 = quotient === -100000;
					// all of these return false
let boolLogical = (bool1 || bool2) && (bool2 || bool4)
console.log(`One of the results is negative: ${boolLogical}`)
// 7. Using comparison and logical operators print out a string in the console that will return a message if one of the results is zero.

// all of this return false.
let value1 = sum === 0; 
let value2 = difference === 0;
let value3 = product === 0;
let value4 = quotient === 0;

let isReturn = (value1 || value2) && (value3 || value4);
console.log(`All of the results are not equal to zero: ${isReturn}`);


