// console.log('Hello from JS');


// //Assignment Operator (=)
// let assignmentNumber = 5;

// // addition assigment operator (+=)

// assignmentNumber += 2;
// console.log(assignmentNumber);

// // Arithmetic operators
// // (+=)
// // (-=)
// // (*=)
// // (/=)

// // subtraction assignment operator (-=).

// // assignmentNumber-= 3;
// // let value = 8;
// // // +=
// // // value+=15;
// // console.log(value)

// // //subtract (-=)
// // // value -=5;
// // console.log(value);

// // //multiplcation *=

// // // value *=2;
// // console.log(value);

// // //division /=
// // value /= 2;
// // console.log(value);

//Arithmetic Operators

let x = 15;
let y = 10;
let sum = x + y;
console.log(sum);

let diff = x - y;
console.log(diff);

let product = x * y;
console.log(product);

let div = x / y;
console.log(div);

let remainder = x % y;
console.log(remainder);

// multiple operators and parethesis PEMDAS rule;

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let z = 1;

// increment and Decrement

// increment
let pre = ++z;
console.log(pre);
console.log(z);

let post = z++;
console.log(post);
console.log(z);

// decrement 

let preD = --z;
console.log(preD);

let postD = z--;
console.log(postD);
console.log(z);

// type Coercion

let numA = 6;
let numB = '6';

console.log(typeof numA);
console.log(typeof numB); //coert or convert number into string data type
 let coercion = numB + numA;

 console.log(coercion);

 let expressionC = 10 + true;
 console.log(expressionC);

 let a = true;
 console.log(typeof a);// bolean

 let b = 10;
 console.log(typeof b);// number

let expressionE = 10 + false;
console.log(expressionE);

let expressionF = true + false;
console.log(expressionF);

let expressionG = 8 + null;
console.log(expressionG);

// Conversion rules
//1. if atleast one operand is an object, it will be converted into a primitive value
//2. after conversion, if at lease 1 operand is a string data type. the 2nd operand is converted into string to perform concatenation.
//3. In other cases where both operands are converted to numbers then an arithmetic operation is executed.

let expressionH = 'batch145' + null;

expressionH = 9 + undefined;
console.log(expressionH);

// Comparison operator
let name = 'Juan';

// EQUALITY operators (==)
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(1 == true);
console.log(1 == false);
console.log(name == 'Juan');
// console.log('Juan' == Juan);


//Developer's tip: upon creating conditions or statement it is strongly recommended to use "strict" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario.

// Relational Operators

let priceA = 1800;
let priceB = 1450;

console.log(priceA < priceB);
console.log(priceA > priceB);

let expressionI = 150 <= 150 ;
console.log(expressionI);

// /Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. it is writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.
// && ampersand
islegalAge = true;
isRegistered = false;

// for the person to be able to vote, both requirments has to be met

let voteR = islegalAge && isRegistered;
console.log(voteR);

// OR || double pipe

let isVaccinated = islegalAge || isRegistered;

console.log(isVaccinated);

// NOT operator ! exclamation
// this will convert/return the opposite value
let isTaken = !true;
let isTalented = false;

console.log(isTaken);
console.log(!isTalented);

